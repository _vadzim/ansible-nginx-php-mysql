Vagrant.configure("2") do |config|
    config.vm.box = "bento/ubuntu-18.04"
    config.vm.hostname = "vm1"
    config.vm.network "private_network", ip: ENV["STATIC_IP"] || "192.168.56.10"
    config.vm.synced_folder "keys/", "/vagrant_keys"
    config.vm.provider "virtualbox" do |vb|
        vb.memory = "2048"
        vb.cpus = 1
    end

    user_name = ENV["USER_NAME"] || "vagrant"
    # add extra ssh key to authorized_keys
    # shell is run by root here
    config.vm.provision "shell", inline: <<-SHELL
        mkdir -p /home/#{user_name}/.ssh
        chmod 700 /home/#{user_name}/.ssh
        cat /vagrant_keys/id_rsa.pub >> /home/#{user_name}/.ssh/authorized_keys
        chmod 600 /home/#{user_name}/.ssh/authorized_keys
    SHELL

    # provision with ansible
    config.vm.provision "ansible" do |ansible|
        ansible.playbook = "play.yaml"
        ansible.extra_vars = {
            user_name: user_name
        }
        ansible.groups = {
            "webserver" => ["default"]
        }
    end
 end