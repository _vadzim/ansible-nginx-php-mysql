## Overview
This project is take home assignment solution given the following requirements:  
![img.png](img.png)  
It uses infrastructure as code to create a Virtualbox guest and  provision it with Nextcloud app that runs in Docker.

## Prerequisites
Must be installed on the machine:  
- ansible
- vagrant
- virtualbox

## Usage
### Start project
```shell
chmod +x start.sh;
 ./start.sh;
```
Project starts on `localhost:8080` 


### Close project  
```shell
vagrant destroy -f
```

### For debugging  
Start only ansible-playbook:  
```shell
ansible-playbook -e "user_name=vagrant" play.yaml
```