#!/bin/bash

# Set default values
DEFAULT_STATIC_IP="192.168.56.10"
DEFAULT_LOCAL_PORT="8080"

# Check if STATIC_IP is provided as a command-line argument
if [ -z "$1" ]; then
    echo "No STATIC_IP provided. Using default: $DEFAULT_STATIC_IP"
    STATIC_IP="$DEFAULT_STATIC_IP"
else
    STATIC_IP="$1"
    echo "Using STATIC_IP: $STATIC_IP"
fi

# Check if local_port is provided as a command-line argument
if [ -z "$2" ]; then
    echo "No local_port provided. Using default: $DEFAULT_LOCAL_PORT"
    local_port="$DEFAULT_LOCAL_PORT"
else
    local_port="$2"
    echo "Using local_port: $local_port"
fi

export STATIC_IP

mkdir -p keys;
key_path="keys/id_rsa";
if [ -f $key_path ]; then
  echo "$key_path exists skipping";
else
  ssh-keygen -t rsa -b 2048 -f "$key_path" -N "" && echo "$key_path created";
fi

export USER_NAME="vagrant"
vagrant up;

# create ssh tunnel from local to virtualbox guest
remote_port="8080";
ssh -o StrictHostKeyChecking=no -i "${key_path}" -L "${local_port}":127.0.0.1:"${remote_port}" -N -f "${USER_NAME}@${STATIC_IP}";
echo "Project available on localhost:$local_port"
